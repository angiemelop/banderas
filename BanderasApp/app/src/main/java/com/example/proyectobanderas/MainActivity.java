package com.example.proyectobanderas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    FirebaseFirestore db;

    private int c=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db= FirebaseFirestore.getInstance();
        //metodo para subir los datos a Cloud Firestore
        //subirDatos();

        recyclerView=(RecyclerView)findViewById(R.id.recycler);

        bajarDatos();

        //recyclerView.setHasFixedSize(true);
        //recyclerView.setLayoutManager(new LinearLayoutManager(this));
        //ArrayList<Pais> paises = listaPaises();
        //RecyclerView.Adapter mAdapter = new Adaptador(paises);
        //recyclerView.setAdapter(mAdapter);

    }
    public void subirDatos(){
        List<Pais> paises= listaPaises();
        for(Pais pais:paises) {
            Map<String, Object> paisesMap = mapa(pais);
            db.collection("Paises").add(paisesMap);
        }
    }
    public Map<String,Object> mapa(Pais paises){
        Map<String,Object> pais=new HashMap<>();
        pais.put("id",c++);
        pais.put("name",paises.getNombrePais());
        pais.put("info",paises.getPoblacion());
        pais.put("foto",paises.getFoto());
        pais.put("url",paises.getVideo());
        pais.put("capital",paises.getCapital());
        return pais;
    }
    public void bajarDatos(){
        db.collection("Paises").orderBy("name").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){

                    List<Pais>paises=new ArrayList<>();

                    for(QueryDocumentSnapshot query:task.getResult()){

                        Map<String,Object> md=query.getData();

                        String name=(String)md.get("name");
                        String info=(String)md.get("info");
                        String foto=(String)md.get("foto");
                        String url=(String)md.get("url");
                        String capital=(String)md.get("capital");

                        Pais pais=new Pais(name,info,foto,url,capital);

                        paises.add(pais);
                    }
                    iniciarRecycler(paises);
                }

            }
        });
    }

    private void iniciarRecycler(List<Pais>paises){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        RecyclerView.Adapter mAdapter = new Adaptador(paises);
        recyclerView.setAdapter(mAdapter);
    }
    private ArrayList<Pais> listaPaises(){
        ArrayList<Pais> paises=new ArrayList<>();

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(getAssets().open("PaisesDatos.csv")));
            String line;
            Log.e("Reader Stuff",reader.readLine());
            while ((line = reader.readLine()) != null) {
                Log.e("code",line);
                String[] d = line.split(";");

                Pais p = new Pais(d[0],d[1],d[2],d[3],d[4]);
                paises.add(p);
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return paises;
    }
}