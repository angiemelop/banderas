package com.example.proyectobanderas;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

public class Adaptador extends RecyclerView.Adapter<Adaptador.MyViewHolder>{
    List<Pais> paisesLista;
    public Adaptador(List<Pais> paisesLista) {
        this.paisesLista = paisesLista;
    }

    @NonNull
    @Override
    public Adaptador.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_items, null, false);
        return new Adaptador.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Pais p= paisesLista.get(position);
        Glide.with(holder.itemView.getContext()).load(p.getFoto()).apply(new RequestOptions().override(360, 330)).into(holder.bandera);
        holder.pob.setText(p.getPoblacion());
        holder.name.setText(p.getNombrePais());
        holder.capital.setText(p.getCapital());
        holder.webView.getSettings().setJavaScriptEnabled(true);
        holder.webView.setWebViewClient(new WebViewClient());
        holder.webView.loadUrl(p.getVideo());

    }



    @Override
    public int getItemCount() {
        return paisesLista.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView pob;
        TextView capital;
        ImageView bandera;
        WebView webView;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name =itemView.findViewById(R.id.idNombre);
            pob =itemView.findViewById(R.id.idInfo);
            bandera =itemView.findViewById(R.id.idImagen);
            capital =itemView.findViewById(R.id.idCapital);
            webView =itemView.findViewById(R.id.webview);
        }
    }

}
