package com.example.proyectobanderas;

public class Pais {
    private String nombrePais;
    private String poblacion;
    private String foto;
    private String video;
    private String capital;
    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }
    public Pais(String nombrePais, String poblacion, String foto, String video, String capital) {
        this.nombrePais = nombrePais;
        this.poblacion = poblacion;
        this.foto = foto;
        this.video = video;
        this.capital=capital;
    }
    public Pais(String nombrePais, String poblacion, String foto) {
        this.nombrePais = nombrePais;
        this.poblacion = poblacion;
        this.foto = foto;

    }

    public Pais() {

    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }



    public String getNombrePais() {
        return nombrePais;
    }

    public void setNombrePais(String nombrePais) {
        this.nombrePais = nombrePais;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
