package com.example.banderawebview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class AdapterBanderas extends RecyclerView.Adapter<AdapterBanderas.MyViewHolder> {
    ArrayList<paises> dataList;
    Context context;

    public AdapterBanderas(ArrayList<paises> dataList, Context context) {
        this.dataList = dataList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_list,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            paises current=dataList.get(position);
            holder.wv.loadUrl(current.getUrl());
        WebViewClient web1=new WebViewClient();
        holder.wv.setWebViewClient(web1);
        WebChromeClient web2=new WebChromeClient();
        holder.wv.setWebChromeClient(web2);
        holder.wv.getSettings().setJavaScriptEnabled(true);
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        WebView wv;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            wv=itemView.findViewById(R.id.webId);
            WebViewClient web1=new WebViewClient();
            wv.setWebViewClient(web1);
            WebChromeClient web2=new WebChromeClient();
            wv.setWebChromeClient(web2);
            wv.getSettings().setJavaScriptEnabled(true);
        }
    }
}
                                                                                                                                                                                                                                                                                   