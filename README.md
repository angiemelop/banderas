# Proyecto Banderas

## Estructura Clases

### Clase Pais Atributos

`private String nombrePais;`  
`private String poblacion;`  
`private String foto;`  
`private String video;`  
`private String capital;`

### Clase MainActivity
`///referencia al id recyclerView`  
`recyclerView=(RecyclerView)findViewById(R.id.recycler);`  
`///setear un LinearLayout al recyclerview`  
`recyclerView.setHasFixedSize(true);`  
`recyclerView.setLayoutManager(new LinearLayoutManager(this));`  
`///llamada a metodo para llenar los datos del archivo .csv`  
`ArrayList<Pais> paises = listaPaises();`    
`///añadir el Adaptador al recyclerview`  
`RecyclerView.Adapter mAdapter = new Adaptador(paises);`  
`recyclerView.setAdapter(mAdapter);`

### Clase Adaptador

 clase Adapter y sus implementaciones :  
`public class Adaptador extends RecyclerView.Adapter<Adaptador.MyViewHolder>{`  
`   @NonNull`  
`   @Override`  
`   public Adaptador.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {`  
`       View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_items, null, false);`  
`       return new Adaptador.MyViewHolder(v);`  
`   }`  
`   @Override`  
`   public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {`  
`   }`  
`   @Override`  
`   public int getItemCount() {`  
`   return paisesLista.size();`  
`   }`  
`   public static class MyViewHolder extends RecyclerView.ViewHolder {`  
`       public MyViewHolder(@NonNull View itemView) {`  
`           super(itemView);`  
`       }`  
`   }`  
`}`

## Crear Archivo csv en el Proyecto
1. crear directorio "assets"
2. crear nuevo archivo con extension .csv

### ingreso de datos en archivo .csv  
`pais;poblacion;bandera;himno;capital`  
`Afghanistan;Población: 37.17 millones;https://www.worldometers.info/img/flags/af-flag.gif;https://www.youtube.com/embed/mjScCkL_ayw;capital: Kabul`

## Recursos Utilizados Glide(app)

`implementation 'androidx.recyclerview:recyclerview:1.1.0'`  
`implementation 'androidx.cardview:cardview:1.0.0'`


## Muestra app

![Texto alternativo](https://gitlab.com/angiemelop/banderas/-/raw/master/imagenes/Captura3.PNG)  

![Texto alternativo](https://gitlab.com/angiemelop/banderas/-/raw/master/imagenes/Captura2.PNG)![Texto alternativo](https://gitlab.com/angiemelop/banderas/-/raw/master/imagenes/Captura1.PNG)










